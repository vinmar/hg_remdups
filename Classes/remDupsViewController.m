//
//  remDupsViewController.m
//  remDups
//
//  Created by vincenzo on 18/01/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "config.h"
#import "FBConnect.h"
#import "remDupsViewController.h"
#import "contactsPhoto.h"

static NSString* kAppId = @"268369801716";

@implementation remDupsViewController


@synthesize facebook = _facebook;
@synthesize cp = _contactsPhoto;



-(IBAction)getContact {
	// creating the picker
	ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
	// place the delegate of the picker to the controll
	picker.peoplePickerDelegate = self;
	
	// showing the picker
	[self presentModalViewController:picker animated:YES];
	// releasing
	[picker release];
}

-(IBAction)getInfoFromFacebook {
//-(IBAction)getInfoFromFacebook :(id)sender{
	
	if (!kAppId) {
		NSLog(@"missing app id!");
		return;
	}
	
	NSLog(@"reqÌuestWithGraphPath");
	[_facebook requestWithGraphPath:@"me/friends" andDelegate:self];
	
}

-(IBAction)checkDups {
	ABAddressBookRef ab = ABAddressBookCreate();
	int len = (int) ABAddressBookGetPersonCount(ab);
	NSLog(@"number of contacts: %d", len);
	
	int i=1;
	int j=1;
	
	NSString *iFName = @"";
	NSString *iLName = @"";
	NSString *jFName = @"";
	NSString *jLName = @"";
	
	for (; i<len+1; i++) {
		//check i contacts with
		 
		ABRecordRef person = ABAddressBookGetPersonWithRecordID(ab,i);
		
		if(person ==  nil){
			i++;
			continue;
		}
		
		iFName = (NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
		iLName = (NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
		//NSLog(@"iFName: %@", iFName);
		//NSLog(@"iLName: %@", iLName);
		
		bool duplicateStatusVector[NUMBER_OF_CHECKS];
		memset(&duplicateStatusVector[0], true, sizeof(duplicateStatusVector[0]) * NUMBER_OF_CHECKS);
		
		for (j=i+1; j<len+1; j++) {
			ABRecordRef person = ABAddressBookGetPersonWithRecordID(ab,(ABRecordID) j);
			
			if(person == nil){
				j++;
				continue;
			}
			
			jFName = (NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
			jLName = (NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
			//with j contact
			//NSLog(@"	jFName: %@", jFName);
			//NSLog(@"	jLName: %@", jLName);
			
			//check First NameÌ
			duplicateStatusVector[FIRST_NAME] = ([iFName isEqualToString: jFName]) ? true : false;
			
		
			//check Last Name
			duplicateStatusVector[LAST_NAME] = ([iLName isEqualToString: jLName]) ? true : false;
			
			//check Mobile
			//duplicateStatusVector[MOBILE] = (iLName == jLName) ? true : false;
			
			//check iPhone
			//duplicateStatusVector[IPHONE] = (iLName == jLName) ? true : false;
			
			
			bool bDuplicate = true;
			
			//NSLog(@"   duplicate vector");
			for (int i=0; i< NUMBER_OF_CHECKS; i++){
				bDuplicate &= duplicateStatusVector[i];
				//NSLog(@"	%d: %d",i,  duplicateStatusVector[i]);
			}
			
			
			if ( bDuplicate )
			{
				NSLog(@"Deleting contact %@, %@",jFName, jLName);
				
				CFErrorRef error = nil;
				//remove the j contacts
				if(!ABAddressBookRemoveRecord(ab, person, &error))
				{
					NSLog(@"Error deleting contact %@, %@ : %@", jFName, jLName, error);
				}
				
				
				//save the address book
				if(!ABAddressBookSave(ab, &error))
				{
					NSLog(@"Error saving addressBook: %@", error);
				}
				
			
				
				//decrese total number of contacts
				len--;
			}
			
		}
	
		
	}

}




- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    // assigning control back to the main controller
	[self dismissModalViewControllerAnimated:YES];
}

- (BOOL)peoplePickerNavigationController: (ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
	
	/*
	// setting the first name
    firstName.text = (NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
	
	// setting the last name
    lastName.text = (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);	
	
	// setting the number

	ABMultiValueRef multi = ABRecordCopyValue(person, kABPersonPhoneProperty);
	number.text = (NSString*)ABMultiValueCopyValueAtIndex(multi, 0);
    */	
	// remove the controller
    [self dismissModalViewControllerAnimated:YES];
	
    return NO;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    return NO;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// FBRequestDelegate

/**
 * Called when the Facebook API request has returned a response. This callback
 * gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
	NSLog(@"received response: %@", response);
};

/**
 * Called when a request returns and its response has been parsed into an object.
 * The resulting object may be a dictionary, an array, a string, or a number, depending
 * on the format of the API response.
 * If you need access to the raw response, use
 * (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response.
 */
- (void)request:(FBRequest *)request didLoad:(id)result {
	if ([result isKindOfClass:[NSArray class]]) {
		result = [result objectAtIndex:0];
		NSLog(@"NSArray class");
	}

	
	if ([result isKindOfClass:[	NSMutableDictionary class]]) {
		
		NSLog(@"NSMutableDictionary class");
		
		NSArray* resultArray = [result allObjects];				
		
		int len = [resultArray count];
		
		NSLog(@"resultArray count : %d ", len);
		
		result = [resultArray objectAtIndex:0];
		len = [result count];
		
		NSLog(@"result count : %d ", len);
		
		len=1;
		for (int i=0; i <len; i++)
		{			
			
			id *obj = [result objectAtIndex:i];
			NSLog(@" NAME %@", [obj valueForKey:@"name"] );
			NSString *s = [NSString stringWithFormat:@"%@",  [obj valueForKey:@"id"]];
			NSString *str = [NSString stringWithFormat:@"http://graph.facebook.com/%@.%@/picture",  [obj valueForKey:@"first_name"], [obj valueForKey:@"last_name"]];
			
			[_contactsPhoto downloadImage:str];
			//NSString *s = [NSString stringWithFormat:@"%@/photos", [obj valueForKey:@"id"]];
			//[_facebook requestWithGraphPath:s andDelegate:_contactsPhoto];
		}
		
	}
	
	
	NSLog(@"didLoad");
};

/**
 * Called when an error prevents the Facebook API request from completing successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
	NSLog(@"didFailWithError: %@", [error localizedDescription]);
};

///////////////////////////////////////////////////////////////////////////////////////////////////
// private

/**
 * Show the authorization dialog.
 */
- (void)login {
	NSLog(@"login");
	[_facebook authorize:_permissions delegate:self];
}

/**
 * Invalidate the access token and clear the cookie.
 */
- (void)logout {
	NSLog(@"logout");
	[_facebook logout:self];
}

///////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Called on a login/logout button click.
 */
- (IBAction)fbButtonClick:(id)sender {
	if (_fbButton.isLoggedIn) {
		[self logout];
	} else {
		[self login];
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Called when the user has logged in successfully.
 */
- (void)fbDidLogin {
	NSLog(@"fbDidLogin!");
	_getInfoFromFacebook.enabled = YES;
	_fbButton.isLoggedIn = YES;
	[_fbButton updateImage];
}

/**
 * Called when the user canceled the authorization dialog.
 */
-(void)fbDidNotLogin:(BOOL)cancelled {
	NSLog(@"did not login");
}

/**
 * Called when the request logout has succeeded.
 */
- (void)fbDidLogout {
	NSLog(@"fbDidLogout!");
	_getInfoFromFacebook.enabled    = NO;
	_fbButton.isLoggedIn         = NO;
	[_fbButton updateImage];
}


///////////////////////////////////////////////////////////////////////////////////////////////////

// FBDialogDelegate

/**
 * Called when a UIServer Dialog successfully return.
 */
- (void)dialogDidComplete:(FBDialog *)dialog {
     NSLog(@"dialogDidComplete");
}



// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		_permissions =  [[NSArray arrayWithObjects:
						  @"read_stream", @"offline_access", @"email", @"friends_photos",nil] retain];
	}
	
	return self;
}


/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/







/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {	
	_facebook = [[Facebook alloc] initWithAppId:kAppId];
	_contactsPhoto = [[contactsPhoto alloc] initWithSession:_facebook];
	_fbButton.isLoggedIn = NO;
	_getInfoFromFacebook.enabled = NO;
	[_fbButton updateImage];
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	[_facebook logout:self];
	[_facebook release];
	[_contactsPhoto release];
}


- (void)dealloc {
    [super dealloc];
}

@end
