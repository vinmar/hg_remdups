//
//  remDupsAppDelegate.m
//  remDups
//
//  Created by vincenzo on 18/01/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "remDupsAppDelegate.h"
#import "remDupsViewController.h"

@implementation remDupsAppDelegate

@synthesize window;
@synthesize viewController;



#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    [self.window addSubview:viewController.view];
    [self.window makeKeyAndVisible];

    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
	return [[viewController facebook] handleOpenURL:url];
}

- (void)dealloc {
	[window release];
	[viewController release];
	[super dealloc];
}



#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}




@end
