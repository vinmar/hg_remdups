//
//  remDupsViewController.h
//  remDups
//
//  Created by vincenzo on 18/01/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
#import "FBLoginButton.h"

@class contactsPhoto;


#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface remDupsViewController : UIViewController  
<FBRequestDelegate,
FBDialogDelegate,
FBSessionDelegate> {
	IBOutlet UIButton *button;
	IBOutlet UILabel *firstName;
	IBOutlet UILabel *lastName;
	IBOutlet UILabel *number;
	IBOutlet UIButton *bCheckDups;
	IBOutlet UIButton *_getInfoFromFacebook;
	
	//facebook
	NSArray* _permissions;
	IBOutlet FBLoginButton* _fbButton;
	Facebook* _facebook;
	
	contactsPhoto* _contactsPhoto;
}

@property(readonly) Facebook *facebook;
@property(readonly) contactsPhoto *cp;



-(IBAction)getContact;
-(IBAction)checkDups;
-(IBAction)fbButtonClick:(id)sender;
//-(IBAction)getInfoFromFacebook:(id)sender;
-(IBAction)getInfoFromFacebook;
@end

