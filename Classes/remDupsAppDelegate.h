//
//  remDupsAppDelegate.h
//  remDups
//
//  Created by vincenzo on 18/01/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "remDupsViewController.h"


@interface remDupsAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    remDupsViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet remDupsViewController *viewController;

@end

